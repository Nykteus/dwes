<?php
    class Empleado{
        var $nombre;
        var $sueldo;
        function __construct($nom, $sue){
            $this->nombre = $nom;
            $this->sueldo = $sue;
            echo("Se ha creado un objeto de tipo empleado<br>");
        }

        function __destruct(){
            echo("<br>Se ha eliminado el objeto empleado.");
        }

        public function imprimir(){
            if($this->sueldo > 3000){
                echo("El trabajador " . $this->nombre . " cobra " . $this->sueldo . " euros y le toca pagar impuestos");
            }
            else{
                echo("El trabajador " . $this->nombre . " cobra " . $this->sueldo . " euros y no le toca pagar impuestos");
            }
        }
    }

    $empleado = new Empleado("Antonio", 3000);
    $empleado->imprimir();
?>
<?php
    class Libro{
        var $titulo;
        var $autor;
        var $anio;
        var $hojas;
        var $editorial;

        function __construct($titulo, $autor, $anio, $hojas, $editorial){
            $this->titulo = $titulo;
            $this->autor = $autor;
            $this->anio = $anio;
            $this->hojas = $hojas;
            $this->editorial = $editorial;
        }

        function imprimir(){
            echo("<h1>Título: $this->titulo </h1>");
            echo("Autor: $this->autor<br>");
            echo("Año de publicación $this->anio<br>");
            echo("Hojas: $this->hojas<br>");
            echo("Editorial $this->editorial<br>");
        }
    }

    $libro = new Libro("Los pilares de la tierra", "Ken Follet", 2001, 1040, "Plaza & Janes Editores");
    $libro->imprimir();
?>
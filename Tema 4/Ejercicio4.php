<?php
class Persona{
        var $nombre;
        var $edad;
        function __construct($nom, $age){
            $this->nombre = $nom;
            $this->edad = $age;
        }

        public function imprimir(){
                echo("Nombre " . $this->nombre . "<br>Edad: " . $this->edad . " años<br>");
        }
    }

    class Empleado extends Persona{
        var $sueldo;
        function __construct($nombre, $edad, $sueldo){
            $this->sueldo = $sueldo;
            parent::__construct($nombre, $edad);
        }

        public function imprimir_sueldo(){
            echo("Sueldo " . $this->sueldo);
        }
    }

    $persona = new Persona("Pepe", 25);
    $empleado = new Empleado("Mario", 25, 2500);
    $persona->imprimir();
    $empleado->imprimir();
    $empleado->imprimir_sueldo();
    ?>
<?php
    class Opcion{
        private $titulo;
        private $enlace;
        private $color;

        public function __construct($titulo, $enlace, $color){
            $this->titulo = $titulo;
            $this->enlace = $enlace;
            $this->color = $color;
        }

        public function graficar(){
            echo("<a style='background-color: $this->color;' href='$this->enlace'>$this->titulo</a> ");
        }
    }

    class Menu{
        private $array = array();
        private $orientacion;

        public function __construct($orienta){
            $this->orientacion = $orienta;
        }

        public function insertar($objeto){
            $this->array[] = $objeto;
        }

        public function graficar(){
            if( strtolower($this->orientacion) == "horizontal"){
                for($x = 0; $x < count($this->array); $x++){
                    $this->array[$x]->graficar();
                }
            }
            else{
                for($x = 0; $x < count($this->array); $x++){
                    $this->array[$x]->graficar();
                    echo "<br>";
                }
            }
        }
    }

    if(isset($_POST["color1"])){
    $menu1 = new Menu($_POST["orientacion"]);
    $objeto1 = new Opcion($_POST["titulo1"] , $_POST["url1"] , $_POST["color1"]);
    $objeto2 = new Opcion($_POST["titulo2"] , $_POST["url2"] , $_POST["color2"]);
    $objeto3 = new Opcion($_POST["titulo3"] , $_POST["url3"] , $_POST["color3"]);
    $menu1->insertar($objeto1);
    $menu1->insertar($objeto2);
    $menu1->insertar($objeto3);
    $menu1->graficar();
    echo("<br>");
    echo("<br><a href='Ejercicio7.php'>Formulario</a>");
    }
    else{
?>
<form action="Ejercicio7.php" method="post">
    TITULO DE LA WEB A MOSTRAR<input type="text" name="titulo1" id="titulo1"/>
    URL1 A MOSTRAR<input type="text" name="url1" id="url1"/>
    COLOR<input type="color" id="color1" name="color1"/><br>
    TITULO DE LA WEB A MOSTRAR<input type="text" name="titulo2" id="titulo2"/>
    URL2 A MOSTRAR<input type="text" name="url2" id="url2"/>
    COLOR<input type="color" id="color2" name="color2"/><br>
    TITULO DE LA WEB A MOSTRAR<input type="text" name="titulo3" id="titulo3"/>
    URL3 A MOSTRAR<input type="text" name="url3" id="url3"/>
    COLOR<input type="color" id="color3" name="color3"/><br>
    <input type="radio" id="orientacion" name="orientacion" value="vertical"/>
    <label for="vertical">Vertical</label><br>
    <input type="radio" id="orientacion" name="orientacion" value="horizontal"/>
    <label for="horizontal">Horizontal</label><br>
    <input type="submit" id="boton" name="boton" value="Generar"/>
</form>
<?php
}
?>
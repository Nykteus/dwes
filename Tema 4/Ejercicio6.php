<?php
    class Opcion{
        private $titulo;
        private $enlace;
        private $color;

        public function __construct($titulo, $enlace, $color){
            $this->titulo = $titulo;
            $this->enlace = $enlace;
            $this->color = $color;
        }

        public function graficar(){
            echo("<p style='background-color: $this->color;'><a href='$this->enlace'>$this->titulo</a> ");
        }
    }

    class Menu{
        private $array = array();
        private $orientacion;

        public function __construct($orienta){
            $this->orientacion = $orienta;
        }

        public function insertar($objeto){
            $this->array[] = $objeto;
        }

        public function graficar(){
            if( strtolower($this->orientacion) == "horizontal"){
                for($x = 0; $x < count($this->array); $x++){
                    $this->array[$x]->graficar();
                }
            }
            else{
                for($x = 0; $x < count($this->array); $x++){
                    $this->array[$x]->graficar();
                    echo "<br>";
                }
            }
        }
    }
    $menu1 = new Menu("vertical");
    $objeto1 = new Opcion("Google" , "http://www.google.es" , "lightblue");
    $objeto2 = new Opcion("Yahoo" , "http://www.yahoo.es" , "green");
    $objeto3 = new Opcion("MSN" , "htpp://www.msn.com" , "lightblue");
    $menu1->insertar($objeto1);
    $menu1->insertar($objeto2);
    $menu1->insertar($objeto3);
    $menu1->graficar();
?>
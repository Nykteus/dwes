<?php
$array = ["Pedro", "Ana", 34, 1];
var_dump($array);
echo "<br/> var_dump() saca los valores del array y el tipo de la variable introducida en cada espacio<br/>";
print_r($array);
echo "<br/> Imprime la posición y el valor sin indicar de que tipo es<br/>";
var_export($array);
echo "<br/> Imprime la posición y el valor indicando con comillas simples si es string o no<br/>";
?>
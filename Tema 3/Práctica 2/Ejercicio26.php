<?php
    $familias = [
        "Los Simpson" => [
            "Padre" => "Homer",
            "Madre" => "Marge",
            "Hijos" => "Bart, Lisa y Maggie"
        ],
        "Los Griffin" => [
            "Padre" => "Peter",
            "Madre" => "Lois",
            "Hijos" => "Chris, Meg y Stewie"
        ]
        ];
        foreach ( $familias as $familia => $miembros){
            echo 'Familia "' . $familia . '": <ul>';
            foreach ( $miembros as $miembro => $nombre){  
                    echo "<li>" . $miembro . " " . $nombre . "</li>";
            }
            echo "</ul><br/>";
            
        }
?>
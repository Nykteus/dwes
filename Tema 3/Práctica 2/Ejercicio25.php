<?php
    $array = [
        5 => 1,
        12 => 2,
        13 => 56,
        "x" => 42
    ];
    echo "El array tiene " . count($array) . " elementos <br/>";
    foreach ( $array as $indice => $valor){
        echo "$indice => $valor <br/>";
    }
    unset($array[5]);
    array_values($array);
    echo "El array tiene " . count($array) . " elementos después del borrado <br/>";
    foreach ( $array as $indice => $valor){
        echo "$indice => $valor <br/>";
    }
    foreach ( $array as $indice => $valor){
        unset($array[$indice]);
    }
    echo "El array tiene " . count($array) . " elementos después del borrado total";
?>
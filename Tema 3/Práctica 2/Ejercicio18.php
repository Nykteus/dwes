<?php
 $lenguajes_cliente = [
    "C1" => "HTML",
    "C2" => "Javascript",
    "C3" => "VBScript"
 ];
 $lenguajes_servidor = [
    "S1" => "PHP",
    "S2" => "Perl",
    "S3" => "CGI"
 ];
 $lenguajes = array_merge($lenguajes_cliente, $lenguajes_servidor);
 foreach( $lenguajes as $indice => $valor){
     echo "$indice => $valor <br/>";
 }
?>
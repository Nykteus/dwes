<?php
    $array = [
        "Primer valor" => 3,
        "Segundo valor" => 2,
        "Tercer valor" => 8,
        "Cuarto valor" => 123,
        "Quinto valor" => 5,
        "Sexto valor" => 1,
    ];
    asort($array);
    echo "Array ordenado de menor a mayor <br/>";
    foreach( $array as $indice => $valor){
        echo "$indice => $valor <br/>";
    }
?>
<?php
if (isset($_POST['nombre'])) {
    $url = $_POST['url'];
    $email = $_POST['mail'];
    $comentario = $_POST['comentario'];
    $name = $_POST['nombre'];
    $gender = $_POST['sexo'];
    function validar_email($email)
    {
        if (empty($_POST['mail'])) {
            $erroremail = "Email is required";
        } else {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $erroremail = "Invalid email format";
            } else {
                $erroremail = "";
            }
        }
        return $erroremail;
    }
    function validar_url($url)
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            $errorurl = "Invalid URL";
        } else {
            $errorurl = "";
        }
        return $errorurl;
    }
    function validar_nombre($name)
    {
        if (empty($name)) {
            $errornombre = "Name is required";
        } else {
            if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
                $errornombre = "Only letters and white space allowed";
            } else {
                $errornombre = "";
            }
        }
        return $errornombre;
    }
    $nameErr = validar_nombre($name);
    $emailErr = validar_email($email);
    $urlErr = validar_url($url);
    if (empty($gender)) {
        $sexErr = "Gender is required";
    } else {
        $sexErr = "";
    }
    if ($nameErr != "" || $emailErr != "" || $sexErr != "") {
?>
        <html lang="es">

        <head>
            <style>
                span {
                    color: red;
                }
            </style>
        </head>

        <body>
            <div>
                <span>* required fields</span>
                <form action="" method="post">
                    Name:<input type="text" name="nombre" value="<?php echo $name ?>"><span>* <?php echo $nameErr ?></span><br />
                    E-mail:<input type="text" name="mail" value="<?php echo $email ?>""><span>* <?php echo $emailErr ?></span><br/>
        Website:<input type=" text" name="url" value="<?php echo $url ?>"><span><?php echo $urlErr ?></span><br />
                    Comment: <input type="text" name="comentario" value="<?php echo $comentario ?>" style="width:200px;height:75px"><br />
                    Gender:<input type="radio" name="sexo" value="mujer" <?php if (
                                                                                isset($gender) &&
                                                                                $gender == "mujer"
                                                                            ) echo "checked"; ?>> Female
                    <input type="radio" name="sexo" value="hombre" <?php if (
                                                                        isset($gender) &&
                                                                        $gender == "hombre"
                                                                    ) echo "checked"; ?>> Male <span>* <?php echo $sexErr ?></span><br />
                    <input type="submit" value="Enviar">
                </form>
            </div>
            <div>
                <h1>Your Inputs</h1>
                <p><?php echo $name ?></p>
                <p><?php echo $email ?></p>
                <p><?php echo $url ?></p>
                <p><?php echo $comentario ?></p>
                <p><?php echo $gender ?></p>
            </div>
        </body>

        </html>
    <?php
    } else {
    ?>
        <html lang="es">

        <head>
            <style>
                span {
                    color: red;
                }
            </style>
        </head>

        <body>
            <div>
                <span>* required fields</span>
                <form action="" method="post">
                    Name:<input type="text" name="nombre" value="<?php echo $name ?>"><span>* <?php echo $nameErr ?></span><br />
                    E-mail:<input type="text" name="mail" value="<?php echo $email ?>""><span>* <?php echo $emailErr ?></span><br/>
        Website:<input type=" text" name="url" value="<?php echo $url ?>"><span><?php echo $urlErr ?></span><br />
                    Comment: <input type="text" name="comentario" value="<?php echo $comentario ?>" style="width:200px;height:75px"><br />
                    Gender:<input type="radio" name="sexo" value="mujer" <?php if (
                                                                                isset($gender) &&
                                                                                $gender == "mujer"
                                                                            ) echo "checked"; ?>> Female
                    <input type="radio" name="sexo" value="hombre" <?php if (
                                                                        isset($gender) &&
                                                                        $gender == "hombre"
                                                                    ) echo "checked"; ?>> Male <span>* <?php echo $sexErr ?></span><br />
                    <input type="submit" value="Enviar">
                </form>
            </div>
            <div>
                <h1>Your Inputs</h1>
                <p><?php echo $name ?></p>
                <p><?php echo $email ?></p>
                <p><?php echo $url ?></p>
                <p><?php echo $comentario ?></p>
                <p><?php echo $gender ?></p>
            </div>
        </body>

        </html>
    <?php
    }
} else {
    ?>
    <html lang="es">

    <head>
        <style>
            span {
                color: red;
            }
        </style>
    </head>

    <body>
        <span>* required fields</span>
        <form action="" method="post">
            Name:<input type="text" name="nombre"><span>*</span><br />
            E-mail:<input type="text" name="mail"><span>*</span><br />
            Website:<input type="text" name="url"><br />
            Comment: <input type="text" name="comentario" style="width:200px;height:75px"><br />
            Gender:<input type="radio" name="sexo" value="mujer"> Female
            <input type="radio" name="sexo" value="hombre"> Male <span>*</span><br />
            <input type="submit" value="Enviar">
        </form>
    </body>

    </html>
<?php
}
?>
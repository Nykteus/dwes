<!DOCTYPE HTML>
<html>

<head>
    <style>
        span {
            color: #FF0000;
        }
    </style>
</head>

<body>
    <?php
    $nameErr = $urlErr = $emailErr = $genderErr = "";
    $name = $url = $email = $gender = $comment = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = $_POST['nombre'];
        $email = $_POST['mail'];
        $url = $_POST['url'];
        $gender = $_POST['sexo'];
        $comment = $_POST['comentario'];
        if (empty($_POST['nombre'])) {
            $nameErr = "Name is required";
        } else {
            if (!preg_match("/^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]*$/", $_POST['nombre'])) {
                $nameErr = "Only letters and white space allowed";
            }
        }
        if (empty($_POST['mail'])) {
            $emailErr = "Email is required";
        } else {
            if (!filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL)) {
                $emailerr = "Invalid email format";
            }
        }
        if (!filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
            $urlErr = "Invalid url format";
        }
        if (empty($_POST['sexo'])) {
            $genderErr = "Gender is required";
        }
    }
    ?>

    <div>
        <span>* required fields</span>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            Name:<input type="text" name="nombre" value="<?php echo $name ?>"><span>* <?php echo $nameErr ?></span><br />
            E-mail:<input type="text" name="mail" value="<?php echo $email ?>"><span>* <?php echo $emailErr ?></span><br/>
        Website:<input type=" text" name="url" value="<?php echo $url ?>"><span><?php echo $urlErr ?></span><br />
            Comment: <input type="text" name="comentario" value="<?php echo $comment ?>" style="width:200px;height:75px"><br />
            Gender:<input type="radio" name="sexo" value="mujer" <?php if (
                                                                        isset($gender) &&
                                                                        $gender == "mujer"
                                                                    ) echo "checked"; ?>> Female
            <input type="radio" name="sexo" value="hombre" <?php if (
                                                                isset($gender) &&
                                                                $gender == "hombre"
                                                            ) echo "checked"; ?>> Male <span>* <?php echo $genderErr ?></span><br />
            <input type="submit" value="Enviar">
        </form>
    </div>
    <div>
        <h1>Your Inputs</h1>
        <p><?php echo $name ?></p>
        <p><?php echo $email ?></p>
        <p><?php echo $url ?></p>
        <p><?php echo $comment ?></p>
        <p><?php echo $gender ?></p>
    </div>

</body>

</html>
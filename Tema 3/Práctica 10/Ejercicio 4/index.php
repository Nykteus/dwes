<?php
session_start();
function votar_azul()
{
    if (isset($_SESSION['azul'])) {
        echo ($_SESSION['azul']);
    } else {
        $_SESSION['azul'] = 0;
        echo ($_SESSION['azul']);
    }
}

function votar_naranja()
{
    if (isset($_SESSION['naranja'])) {
        echo ($_SESSION['naranja']);
    } else {
        $_SESSION['naranja'] = 0;
        echo ($_SESSION['naranja']);
    }
}
?>

<html>

<head>
    <style>
    </style>
</head>

<body>
    <form id="formulario" name="formulario" action="votar.php" method="post">
        <button type="submit" name="boton" value="azul" style="font-size: 60px; line-height: 50px; color: hsl(200, 100%, 50%);">✔</button>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="<?php votar_azul() ?>" height="50">
            <line x1="0" y1="25" x2="<?php votar_azul() ?>" y2="25" stroke="hsl(200, 100%, 50%)" stroke-width="50"></line>
        </svg><br>
        <button type="submit" name="boton" value="naranja" style="font-size: 60px; line-height: 50px; color: hsl(35, 100%, 50%)">✔</button></td>

        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="<?php votar_naranja() ?>" height="50">
            <line x1="0" y1="25" x2="<?php votar_naranja() ?>" y2="25" stroke="hsl(35, 100%, 50%)" stroke-width="50"></line>
        </svg><br>
        <button type="submit" id="cero" name="boton" value="centro" style="line-height: 40px;">Poner a cero</button>
    </form>
</body>

</html>
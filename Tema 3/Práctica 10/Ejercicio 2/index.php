<?php
function posicion()
{
    session_start();
    if (isset($_SESSION['posicion'])) {
        echo ($_SESSION['posicion']);
    } else {
        $_SESSION['posicion'] = 0;
        echo ($_SESSION['posicion']);
    }
}
?>

<html>

<head>
    <style>
        #cero{
            margin-left: 250px;
        }
        #menos,#mas{
            margin-left: 150px;
        }
    </style>
</head>

<body>
    <form id="formulario" name="formulario" action="movimiento.php" method="post">
        <button type="submit" id="menos" name="boton" value="izquierda" style="font-size: 60px; line-height: 40px;">☜</button>
        <button type="submit" id="mas" name="boton" value="derecha" style="font-size: 60px; line-height: 40px;">☞</button><br>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="600" height="20" viewBox="-300 0 600 20">
            <line x1="-300" y1="10" x2="300" y2="10" stroke="black" stroke-width="5"></line>
            <circle cx="<?php posicion() ?>" cy="10" r="8" fill="red"></circle>
        </svg><br>
        <button type="submit" id="cero" name="boton" value="centro">Poner a cero</button>
    </form>
</body>

</html>
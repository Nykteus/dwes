<?php
session_start();
function lateral()
{
    if (isset($_SESSION['x'])) {
        echo ($_SESSION['x']);
    } else {
        $_SESSION['x'] = 0;
        echo ($_SESSION['x']);
    }
}

function vertical()
{
    if (isset($_SESSION['y'])) {
        echo ($_SESSION['y']);
    } else {
        $_SESSION['y'] = 0;
        echo ($_SESSION['y']);
    }
}
?>

<html>

<head>
    <style>
        #arriba{
            margin-left: 90px;
        }
        #abajo{
            margin-left: 90px;
        }
    </style>
</head>

<body>
    <form id="formulario" name="formulario" action="movimiento.php" method="post">
        <button type="submit" id="arriba" name="boton" value="arriba" style="font-size: 60px; line-height: 60px;">👆</button><br>
        <button type="submit" id="menos" name="boton" value="izquierda" style="font-size: 60px; line-height: 40px;">☜</button>
        <button type="submit" id="cero" name="boton" value="centro" style="line-height: 40px;">Volver al centro</button>
        <button type="submit" id="mas" name="boton" value="derecha" style="font-size: 60px; line-height: 40px;">☞</button><br>
        <button type="submit" id="abajo" name="boton" value="abajo" style="font-size: 60px; line-height: 60px;">👇</button><br>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="400" height="400" viewBox="-200 -200 400 400" style="border: black 2px solid">
            <circle cx="<?php lateral() ?>" cy="<?php vertical() ?>" r="8" fill="red"></circle>
        </svg>
    </form> 
    <?php
    var_dump($_SESSION['y']);
    ?>
    </body>

</html>
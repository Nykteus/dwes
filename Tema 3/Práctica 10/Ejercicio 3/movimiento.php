<?php
if (isset($_POST['boton'])) {
    session_start();
    $opcion = $_POST['boton'];
    switch ($opcion) {
        case "izquierda": {
                if ($_SESSION['x'] > -200) {
                    $_SESSION['x'] -= 20;
                }
            }
            break;
        case "derecha": {
                if ($_SESSION['x'] < 200) {
                    $_SESSION['x'] += 20;
                }
            }
            break;
        case "arriba": {
                if ($_SESSION['y'] > -200) {
                    $_SESSION['y'] -= 20;
                }
            }
            break;
        case "abajo": {
                if ($_SESSION['y'] < 200) {
                    $_SESSION['y'] += 20;
                }
            }
            break;
        case "centro": {
                $_SESSION['x'] = 0;
                $_SESSION['y'] = 0;
            }
            break;
    }
    header("location:index.php");
}
?>
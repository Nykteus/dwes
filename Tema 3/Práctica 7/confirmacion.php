<?php
    if ( isset($_POST['usuario']) && isset($_POST['contra'])){
        session_start();
        if(empty($_POST['usuario'])){
            $usuario = "Usuario no introducido";
        }else{
            $usuario = $_POST['usuario'];
        }
        if(empty($_POST['contra'])){
            $contra = "Contraseña no introducido";
        }else{
            $contra = $_POST['contra'];
        }
        $_SESSION['usuario'] = $usuario;
        $_SESSION['contra'] = $contra;
        header("location:login.php");
    }
        ?>
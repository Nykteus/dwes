<?php
echo '<style>
        table,tr,td{
            border: 1px solid black;
            border-collapse: collapse;
            text-align: center;
        }
      </style>';
    if( isset($_POST['mes'])){
        $mes = $_POST['mes'];
        $mesnombre = $mes - 1;
        $anio = $_POST['anio'];
        $diaSemana=date("w",mktime(0,0,0,$mes,1,$anio))-1;
        $contador = 1;
        $meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        $dia = ["L", "M", "X", "J", "V", "S", "D"];
        echo "$meses[$mesnombre] del $anio";
            echo '<table style="border: 1px solid black;">';
            echo '<tr>';
            for ($t = 0; $t < 7; $t++){
                echo "<td width='50px;'>$dia[$t]</td>";
            }
            echo "</tr>";
            echo '<tr>';
            if($diaSemana < 0){
                for ($r = 1; $r < 8; $r++){
                    if($r >= 7){
                        echo '<td></td>';
                    }
                    else{
                        echo "<td width='50px;'>$contador</td>";
                        $contador++;
                    }
            }
        }else{
                for ($r = 1; $r < 8; $r++){
                    if( $r <= $diaSemana){
                        echo "<td width='50px;'></td>";
                    }
                    else{
                        echo "<td width='50px;'>$contador</td>";
                        $contador++;
                }
            }
        }
            echo "</tr>";
            for ( $x = 0; $x < 3; $x++){
                    echo '<tr style="border: 1px solid black;">';
                    for ($i = 1; $i < 8; $i++){
                        echo "<td width='50px;'>$contador</td>";
                        $contador++;
                    }
                    echo "</tr>";
                }
                switch ($mes){
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:{
                        if(($diaSemana < 0) || $diaSemana ==5){
                            echo "<tr>";
                            for ($i = 0; $i < 7; $i++){
                                echo "<td width='50px;'>$contador</td>";
                                $contador++;
                            }
                            echo "</tr>";
                            echo "<tr>";
                            while( $contador < 32){
                                echo "<td width='50px;'>$contador</td>";
                                $contador++;
                            }
                            echo "</tr>";
                            }else {
                                    echo "<tr>";
                                    while( $contador < 32){
                                        echo "<td width='50px;'>$contador</td>";
                                        $contador++;
                            }
                        echo "</tr>";
                }
                        }
                    break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:{
                        if(($diaSemana < 0) || $diaSemana ==5){
                            echo "<tr>";
                            for ($i = 0; $i < 7; $i++){
                                echo "<td width='50px;'>$contador</td>";
                                $contador++;
                            }
                            echo "</tr>";
                            echo "<tr>";
                            while( $contador < 31){
                                echo "<td width='50px;'>$contador</td>";
                                $contador++;
                            }
                            echo "</tr>";
                            }else{
                                echo "<tr>";
                                while( $contador < 31){
                                    echo "<td width='50px;'>$contador</td>";
                                    $contador++;
                        }
                        echo "</tr>";
                        }
                    }
                    break;
                    case 2:{
                        if( ($anio % 4 == 0) || ($anio % 400 == 0)){
                            echo "<tr>";
                            while($contador < 30){
                            echo "<td width='50px;'>$contador</td>";
                            $contador++;
                            }
                            echo "</tr>";
                            }
                            else{
                                echo "<tr>";
                            while($contador < 29){
                            echo "<td width='50px;'>$contador</td>";
                            $contador++;
                            }
                            echo "</tr>";
                            }
                        }
                    break;
                    }
                    echo "</table>";
    }
    else{
        ?>
        <html>
            <body>
                <form action="" method="post">
                    <label for="mes">Escoge un mes:</label>
                    <select id="mes" name="mes">
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>
                    </select>
                    Introduce el año: <input type="text" name="anio" value="2020">
                    <input type="submit" value="Enviar">
                </form>
            </body>
        </html>
        <?php
    }
?>
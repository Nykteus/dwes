function GetXMLHTTP() {
    var obj = null;

    try { obj = new ActiveXObject("Msxml2.XMLHTTP") } 	// Funcionará para JavaScript 5.0
    catch (e) {
        try {
            obj = new ActiveXObject("Microsoft.XMLHTTP");	 // Probamos con IE
        } catch (e2) {
            obj = null;
        }
    }
    if (!obj && typeof XMLHttpRequest != "undefined") {  		// Otros navegadores
        obj = new XMLHttpRequest();
    }
    return obj;
}

function inicio() {
    objAjax = GetXMLHTTP();
    objAjax.open("POST", "php.php", true);

    objAjax.onreadystatechange = function () {
        if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
            if (objAjax.responseText == 0) {
                document.getElementById("form_login").style.display = "block";
                document.getElementById("art_mostrar").style.display = "none";
            }
            else {
                document.getElementById("form_login").style.display = "none";
                document.getElementById("art_mostrar").innerHTML = objAjax.responseText;
                document.getElementById("art_mostrar").style.display = "block";
            }
        }
    }
    objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    objAjax.send("inicio=" + 0);
}

function logeo() {
    objAjax = GetXMLHTTP();
    objAjax.open("POST", "php.php", true);

    objAjax.onreadystatechange = function () {
        if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
            if (objAjax.responseText != 1) {
                document.getElementById("form_login").style.display = "none";
                document.getElementById("art_mostrar").innerHTML = objAjax.responseText;
                document.getElementById("art_mostrar").style.display = "block";
            }
            else {
                document.getElementById("sp_pass").style.display = "block";
            }
        }
    }
    objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    objAjax.send("login=" + document.getElementById("login").value + "&password=" + document.getElementById("pass").value);
} //función para logear al paciente

function alta(opcion) {
    if (opcion == 0) {
        document.getElementById("art_mostrar").innerHTML = "<h1>INSERTAR PACIENTE</h1>";
        document.getElementById("art_mostrar").innerHTML += "<form id='alt_pac' name='alt_pac'>";
        document.getElementById("alt_pac").innerHTML += "<span class='rojo'>Identificación</span><input type='text' id='pac_dni' name='pac_dni' onblur='buscar(1); validar(5)' required/><span id='sp_id_pac' style='display: none; color: red;'>Ese DNI ya está registrado</span><span id='sp_id_pac_val' style='display: none; color: red;'>DNI inválido</span><br>";
        document.getElementById("alt_pac").innerHTML += "<span class='rojo'>Nombre</span><input type='text' id='pac_name' name='pac_name' required/><br>";
        document.getElementById("alt_pac").innerHTML += "<span class='rojo'>Apellidos</span><input type='text' id='pac_apell' name='pac_apell' onblur='validar(1)' required/><span id='sp_nombre_pac' style='display: none; color: red;'>El nombre no es válido</span><br>";
        document.getElementById("alt_pac").innerHTML += "<span class='rojo'>Fecha de Nacimiento</span><input type='date' id='pac_nac' name='pac_nac' required/><br>";
        document.getElementById("alt_pac").innerHTML += "<span class='rojo'>Sexo</span><select id='pac_sexo' name='pac_sexo' required>";
        document.getElementById("pac_sexo").innerHTML += "<option value='masculino'>Masculino</option>";
        document.getElementById("pac_sexo").innerHTML += "<option value='femenino'>Femenino</option>";
        document.getElementById("alt_pac").innerHTML += "</select><br>"
        document.getElementById("alt_pac").innerHTML += "<span class='rojo'>Nombre de Usuario</span><input type='text' id='pac_login' name='pac_login' onblur='buscar(2)' required/><span id='sp_usu_pac' style='display: none; color: red;'>Ese usuario ya existe</span><br>";
        document.getElementById("alt_pac").innerHTML += "<span class='rojo'>Contraseña</span><input type='password' id='pac_pass' name='pac_pass' required/><br>";
        document.getElementById("alt_pac").innerHTML += "<button type='button' id='add_pac_but' name='add_pac_but' onclick='introducir(0)'>Enviar</button>";
        document.getElementById("art_mostrar").innerHTML += "</form>";
    }
    else {
        if (opcion == 1) {
            document.getElementById("art_mostrar").innerHTML = "<h1>Formulario de Registro de Médicos</h1>";
            document.getElementById("art_mostrar").innerHTML += "<form id='alt_med' name='alt_med'>";
            document.getElementById("alt_med").innerHTML += "<span class='rojo'>Nombre</span><input type='text' id='med_name' name='med_name' required/><br>";
            document.getElementById("alt_med").innerHTML += "<span class='rojo'>Apellidos</span><input type='text' id='med_apell' name='med_apell' onblur='validar(2)' required/><span id='sp_nombre_med' style='display: none; color: red;'>El nombre no es válido</span><br>";
            document.getElementById("alt_med").innerHTML += "<span class='rojo'>Especialidad</span><input type='text' id='med_esp' name='med_esp' onblur='validar(3)' required/><span id='sp_esp_med' style='display: none; color: red;'>El nombre de la especialidadno es válido</span><br>";
            document.getElementById("alt_med").innerHTML += "<span class='rojo'>Teléfono</span><input type='text' id='med_tel' name='med_tel' onblur='validar(4)' required/><span id='sp_esp_tel' style='display: none; color: red;'>Teléfono inválido</span><br>";
            document.getElementById("alt_med").innerHTML += "<span class='rojo'>Email</span><input type='text' id='med_mail' name='med_mail' onblur='validar(0)' required/><span id='sp_mail_med' style='display: none; color: red;'>Email Incorrecto</span><br>";
            document.getElementById("alt_med").innerHTML += "<span class='rojo'>DNI</span><input type='text' id='med_dni' name='med_dni' onblur='buscar(3); validar(6)' required/><span id='sp_id_med' style='display: none; color: red;'>Ese DNI ya está registrado</span><span id='sp_id_med_val' style='display: none; color: red;'>DNI inválido</span><br>";
            document.getElementById("alt_med").innerHTML += "<span class='rojo'>Nombre de Usuario</span><input type='text' id='med_login' name='med_login' onblur='buscar(4)' required/><span id='sp_usu_med' style='display: none; color: red;'>Ese usuario ya existe</span><br>";
            document.getElementById("alt_med").innerHTML += "<span class='rojo'>Contraseña</span><input type='password' id='med_pass' name='med_pass' required/><br>";
            document.getElementById("alt_med").innerHTML += "<button type='button' id='add_med_but' name='add_med_but' onclick='introducir(1)'>Enviar</button>";
            document.getElementById("art_mostrar").innerHTML += "</form>";
        }
        else {
            var pacientes = [];
            var medicos = [];
            var consultorios = [];
            var contador_med = contador_pac = contador_consul = 0;
            objAjax = GetXMLHTTP();
            objAjax.open("POST", "php.php", true);
            objAjax.onreadystatechange = function () {
                if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                    var resultados = JSON.parse(objAjax.responseText);
                    for (valor in resultados) {
                        if (valor == "Pacientes") {
                            for (x = 0; x < Object.keys(resultados[valor]).length; x++) {
                                pacientes.push(resultados[valor][x]);
                                contador_pac++;
                            }
                        } else {
                            if (valor == "Medicos") {
                                for (x = 0; x < Object.keys(resultados[valor]).length; x++) {
                                    medicos.push(resultados[valor][x]);
                                    contador_med++;
                                }
                            }
                            else {
                                for (x = 0; x < Object.keys(resultados[valor]).length; x++) {
                                    consultorios.push(resultados[valor][x]);
                                    contador_consul++;
                                }
                            }
                        }
                    }
                    document.getElementById("art_mostrar").innerHTML = "<h1>ASIGNAR CITA</h1>";
                    document.getElementById("art_mostrar").innerHTML += "<form id='asig_cita' name='asig_cita'>";
                    document.getElementById("asig_cita").innerHTML += "<span class='rojo'>Paciente</span><select id='cit_name' name='cit_name' required>";
                    document.getElementById("cit_name").innerHTML += "<option value='' selected disabled hidden>Selecciona</option>";
                    for (x = 0; x < contador_pac; x++) {
                        document.getElementById("cit_name").innerHTML += "<option value='" + pacientes[x][0] + "'>" + pacientes[x][1] + " " + pacientes[x][2] + "</option>";
                    }
                    document.getElementById("asig_cita").innerHTML += "</select><br>";
                    document.getElementById("asig_cita").innerHTML += "<span class='rojo'>Fecha</span><input type='date' id='cita_fecha' name='cita_fecha' required/><br>";
                    document.getElementById("asig_cita").innerHTML += "<span class='rojo'>Hora</span><input type='time' id='hora_cita' name='hora_cita' required/><br>";
                    document.getElementById("asig_cita").innerHTML += "<span class='rojo'>Médico</span><select id='cit_med' name='cit_med' required>";
                    document.getElementById("cit_med").innerHTML += "<option value='' selected disabled hidden>Selecciona</option>";
                    for (x = 0; x < contador_med; x++) {
                        document.getElementById("cit_med").innerHTML += "<option value='" + medicos[x][0] + "'>" + medicos[x][1] + " " + medicos[x][2] + "</option>";
                    }
                    document.getElementById("asig_cita").innerHTML += "</select><br>";
                    document.getElementById("asig_cita").innerHTML += "<span class='rojo'>Consultorio</span><select id='cit_consul' name='cit_consul' required>";
                    document.getElementById("cit_consul").innerHTML += "<option value='' selected disabled hidden>Selecciona</option>";
                    for (x = 0; x < contador_consul; x++) {
                        document.getElementById("cit_consul").innerHTML += "<option value='" + consultorios[x][0] + "'>" + consultorios[x][1] + "</option>";
                    }
                    document.getElementById("asig_cita").innerHTML += "</select><br>";
                    document.getElementById("asig_cita").innerHTML += "<button type='button' id='add_cit_but' name='add_cit_but' onclick='introducir(2)'>Enviar</button>";
                    document.getElementById("art_mostrar").innerHTML += "</form>";
                }
            }
            objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            objAjax.send("cita=" + 0);
        }
    }
}//función para generar los formularios de alta de paciente, médicos y citas

function introducir(opcion) {
    objAjax = GetXMLHTTP();
    objAjax.open("POST", "php.php", true);
    if (opcion == 0) {
        if (document.getElementById("pac_name").value && document.getElementById("pac_apell").value && document.getElementById("pac_dni").value && document.getElementById("pac_nac").value && document.getElementById("pac_sexo").value && document.getElementById("pac_login").value && document.getElementById("pac_pass").value && Date.parse(document.getElementById("pac_nac").value) < Date.now()) {
            objAjax.onreadystatechange = function () {
                if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                    document.getElementById("form_login").style.display = "none";
                    document.getElementById("art_mostrar").style.display = "block";
                    document.getElementById("art_mostrar").innerHTML = "Paciente introducido";
                    document.getElementById("art_mostrar").innerHTML += objAjax.responseText;
                }
            }
            objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            objAjax.send("dni=" + document.getElementById("pac_dni").value + "&nombre=" + document.getElementById("pac_name").value + "&apellidos=" + document.getElementById("pac_apell").value + "&fecha=" + document.getElementById("pac_nac").value + "&sexo=" + document.getElementById("pac_sexo").value + "&usuario=" + document.getElementById("pac_login").value + "&contra=" + document.getElementById("pac_pass").value);
        }

    }
    else {
        if (opcion == 1) {
            if (document.getElementById("med_name").value && document.getElementById("med_apell").value && document.getElementById("med_dni").value && document.getElementById("med_mail").value && document.getElementById("med_esp").value && document.getElementById("med_login").value && document.getElementById("med_pass").value && document.getElementById("med_tel").value) {
                objAjax.onreadystatechange = function () {
                    if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                        document.getElementById("form_login").style.display = "none";
                        document.getElementById("art_mostrar").style.display = "block";
                        document.getElementById("art_mostrar").innerHTML = "Médico introducido";
                        document.getElementById("art_mostrar").innerHTML += objAjax.responseText;
                    }
                }
                objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                objAjax.send("dni=" + document.getElementById("med_dni").value + "&nombre=" + document.getElementById("med_name").value + "&apellidos=" + document.getElementById("med_apell").value + "&especialidad=" + document.getElementById("med_esp").value + "&mail=" + document.getElementById("med_mail").value + "&usuario=" + document.getElementById("med_login").value + "&contra=" + document.getElementById("med_pass").value + "&telefono=" + document.getElementById("med_tel").value);
            }
        }
        else {
            objAjax.onreadystatechange = function () {
                if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                    document.getElementById("form_login").style.display = "none";
                    document.getElementById("art_mostrar").style.display = "block";
                    document.getElementById("art_mostrar").innerHTML = "Cita introducida";
                    document.getElementById("art_mostrar").innerHTML += objAjax.responseText;
                }
            }
            objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            objAjax.send("pacientecita=" + document.getElementById("cit_name").value + "&fechacita=" + document.getElementById("cita_fecha").value + "&hora=" + document.getElementById("hora_cita").value + "&medicocita=" + document.getElementById("cit_med").value + "&consultoriocita=" + document.getElementById("cit_consul").value);

        }
    }
} //función para pasar los datos a php mediante ajax y añadirlo a la base de datos, la opción 0 para pacientes, 1 para médicos y 2 para citas

function logout() {
    objAjax = GetXMLHTTP();
    objAjax.open("POST", "php.php", true);

    objAjax.onreadystatechange = function () {
        if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
            document.getElementById("form_login").style.display = "block";
            document.getElementById("art_mostrar").style.display = "none";
            document.getElementById("login").value = "";
            document.getElementById("pass").value = "";
        }
    }
    objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    objAjax.send("logout=" + 0);
} //función para cerrar la sesión y borrar los datos de conexión de ese usuario

function buscar(opcion) {
    objAjax = GetXMLHTTP();
    objAjax.open("POST", "php.php", true);
    if (opcion == 0) {
        objAjax.onreadystatechange = function () {
            if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                var resultado = objAjax.responseText;
                if (resultado == 0) {
                    document.getElementById("sp_log").style.display = "block";
                    document.getElementById("boton_login").disabled = true;
                } else {
                    document.getElementById("sp_log").style.display = "none";
                    document.getElementById("boton_login").disabled = false;
                }
            }
        }
        objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        objAjax.send("buscalogin=" + document.getElementById("login").value + "&eleccion=" + 0);
    } else {
        if (opcion == 1) {
            objAjax.onreadystatechange = function () {
                if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                    var resultado = objAjax.responseText;
                    if (resultado == 0) {
                        document.getElementById("sp_id_pac").style.display = "block";
                        document.getElementById("add_pac_but").disabled = true;
                    } else {
                        document.getElementById("sp_id_pac").style.display = "none";
                        document.getElementById("add_pac_but").disabled = false;
                    }
                }
            }
            objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            objAjax.send("buscalogin=" + document.getElementById("pac_dni").value + "&eleccion=" + 1);
        }
        else {
            if (opcion == 2) {
                objAjax.onreadystatechange = function () {
                    if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                        var resultado = objAjax.responseText;
                        if (resultado == 0) {
                            document.getElementById("sp_usu_pac").style.display = "block";
                            document.getElementById("add_pac_but").disabled = true;
                        } else {
                            document.getElementById("sp_usu_pac").style.display = "none";
                            document.getElementById("add_pac_but").disabled = false;
                        }
                    }
                }
                objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                objAjax.send("buscalogin=" + document.getElementById("pac_login").value + "&eleccion=" + 2);
            }
            else {
                if (opcion == 3) {
                    objAjax.onreadystatechange = function () {
                        if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                            var resultado = objAjax.responseText;
                            if (resultado == 0) {
                                document.getElementById("sp_id_med").style.display = "block";
                                document.getElementById("add_med_but").disabled = true;
                            } else {
                                document.getElementById("sp_id_med").style.display = "none";
                                document.getElementById("add_med_but").disabled = false;
                            }
                        }
                    }
                    objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    objAjax.send("buscalogin=" + document.getElementById("med_dni").value + "&eleccion=" + 3);
                }
                else {
                    objAjax.onreadystatechange = function () {
                        if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                            var resultado = objAjax.responseText;
                            if (resultado == 0) {
                                document.getElementById("sp_usu_med").style.display = "block";
                                document.getElementById("add_med_but").disabled = true;
                            } else {
                                document.getElementById("sp_usu_med").style.display = "none";
                                document.getElementById("add_med_but").disabled = false;
                            }
                        }
                    }
                    objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    objAjax.send("buscalogin=" + document.getElementById("med_login").value + "&eleccion=" + 4);
                }
            }
        }
    }
}
//realizará la búsqueda de usuarios, dnis y si meto esa posibilidad de contraseñas para validar que existen en la base de datos, para el valor 0 hará la búsqueda desde la ventana de login (comprobar que existe el usuario) y para el valor 2 lo hará desde el formulario de alta (para ver si ya existe y no se puede crear otro)

function mostrar(opcion) {
    switch (opcion) {
        case 0: {
            objAjax = GetXMLHTTP();
            objAjax.open("POST", "php.php", true);

            objAjax.onreadystatechange = function () {
                if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                    var citas = JSON.parse(objAjax.responseText);
                    var contador = 0;
                    document.getElementById("art_mostrar").innerHTML = "<h1>LISTADO DE CITAS ATENDIDAS</h1>";
                    document.getElementById("art_mostrar").innerHTML += "<table id='tab_cit_at' name='tab_cit_at'>";
                    document.getElementById("tab_cit_at").innerHTML = "<tr id='" + contador + "'>";
                    document.getElementById(contador).innerHTML = "<td>Fecha</td>";
                    document.getElementById(contador).innerHTML += "<td>Hora</td>";
                    document.getElementById(contador).innerHTML += "<td colspan='2'>Paciente</td>";
                    document.getElementById(contador).innerHTML += "<td colspan='2'>Médico</td>";
                    document.getElementById(contador).innerHTML += "<td>Consultorio</td>";
                    document.getElementById(contador).innerHTML += "<td>Estado</td>";
                    document.getElementById("tab_cit_at").innerHTML += "</tr>";
                    contador++;
                    for (x = 0; x < citas.length; x++) {
                        document.getElementById("tab_cit_at").innerHTML += "<tr id='" + contador + "'>"
                        for (y = 0; y < citas[x].length - 1; y++) {
                            document.getElementById(contador).innerHTML += "<td>" + citas[x][y] + "</td>"
                        }
                        contador++;
                    }
                    document.getElementById("art_mostrar").innerHTML += "</table>";
                }
            }
            objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            objAjax.send("mostrar=" + 0);
        }
            break;
        case 1: {
            objAjax = GetXMLHTTP();
            objAjax.open("POST", "php.php", true);

            objAjax.onreadystatechange = function () {
                if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                    document.getElementById("art_mostrar").innerHTML = "<h1>LISTADO DE CITAS POR ATENDER</h1>"
                    document.getElementById("art_mostrar").innerHTML += "<table id='tab_cit_pen' name='tab_cit_pen'>";
                    var citas = JSON.parse(this.responseText);
                    var contador = 0;
                    document.getElementById("tab_cit_pen").innerHTML = "<tr id='" + contador + "'>";
                    document.getElementById(contador).innerHTML = "<td>Fecha</td>";
                    document.getElementById(contador).innerHTML += "<td>Hora</td>";
                    document.getElementById(contador).innerHTML += "<td colspan='2'>Paciente</td>";
                    document.getElementById(contador).innerHTML += "<td colspan='2'>Médico</td>";
                    document.getElementById(contador).innerHTML += "<td>Consultorio</td>";
                    document.getElementById(contador).innerHTML += "<td>Estado</td>";
                    document.getElementById(contador).innerHTML += "<td>Atender</td>";
                    document.getElementById("tab_cit_pen").innerHTML += "</tr>";
                    contador++;
                    for (x = 0; x < citas.length; x++) {
                        document.getElementById("tab_cit_pen").innerHTML += "<tr id='" + contador + "'>"
                        for (y = 0; y < citas[x].length - 1; y++) {
                            document.getElementById(contador).innerHTML += "<td>" + citas[x][y] + "</td>";
                            if (y == citas[x].length - 2) {
                                document.getElementById(contador).innerHTML += "<td><button type='button' id='but_asig' name='but_asig' onclick='atender(" + citas[x][y + 1] + ")'>X</button></td>"
                            }
                        }
                        contador++;
                    }
                    document.getElementById("art_mostrar").innerHTML += "</table>";
                }
            }
            objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            objAjax.send("mostrar=" + 1);
        }
            break;
        case 2: {
            objAjax = GetXMLHTTP();
            objAjax.open("POST", "php.php", true);

            objAjax.onreadystatechange = function () {
                if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                    document.getElementById("art_mostrar").innerHTML = "<h1>TUS CITAS</h1>"
                    document.getElementById("art_mostrar").innerHTML += "<table id='tab_cit_pac' name='tab_cit_pac'>";
                    var citas = JSON.parse(this.responseText);
                    var contador = 0;
                    document.getElementById("tab_cit_pac").innerHTML = "<tr id='" + contador + "'>";
                    document.getElementById(contador).innerHTML = "<td>Fecha</td>";
                    document.getElementById(contador).innerHTML += "<td>Hora</td>";
                    document.getElementById(contador).innerHTML += "<td colspan='2'>Paciente</td>";
                    document.getElementById(contador).innerHTML += "<td colspan='2'>Médico</td>";
                    document.getElementById(contador).innerHTML += "<td>Consultorio</td>";
                    document.getElementById(contador).innerHTML += "<td>Estado</td>";
                    document.getElementById(contador).innerHTML += "<td>Observaciones</td>";
                    document.getElementById("tab_cit_pac").innerHTML += "</tr>";
                    contador++;
                    for (x = 0; x < citas.length; x++) {
                        document.getElementById("tab_cit_pac").innerHTML += "<tr id='" + contador + "'>"
                        for (y = 0; y < citas[x].length; y++) {
                            document.getElementById(contador).innerHTML += "<td>" + citas[x][y] + "</td>"
                        }
                        document.getElementById("tab_cit_pac").innerHTML += "</tr>";
                        contador++;
                    }
                    document.getElementById("art_mostrar").innerHTML += "</table>";
                }
            }
            objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            objAjax.send("mostrar=" + 2);
        }
            break;
        case 3: {
            objAjax = GetXMLHTTP();
            objAjax.open("POST", "php.php", true);

            objAjax.onreadystatechange = function () {
                if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                    document.getElementById("art_mostrar").innerHTML = "<h1>LISTADO DE PACIENTES</h1>"
                    document.getElementById("art_mostrar").innerHTML += "<table id='tab_pac' name='tab_pac'>";
                    var pacientes = JSON.parse(objAjax.responseText);
                    var contador = 0;
                    document.getElementById("tab_pac").innerHTML = "<tr id='" + contador + "'>";
                    document.getElementById(contador).innerHTML = "<td>Identificación</td>";
                    document.getElementById(contador).innerHTML += "<td>Nombre</td>";
                    document.getElementById(contador).innerHTML += "<td>Apellido</td>";
                    document.getElementById(contador).innerHTML += "<td>Fecha de Nacimiento</td>";
                    document.getElementById(contador).innerHTML += "<td>Sexo</td>";
                    document.getElementById("tab_pac").innerHTML += "</tr>";
                    contador++;
                    for (x = 0; x < pacientes.length; x++) {
                        document.getElementById("tab_pac").innerHTML += "<tr id='" + contador + "'>"
                        for (y = 0; y < pacientes[x].length; y++) {
                            document.getElementById(contador).innerHTML += "<td>" + pacientes[x][y] + "</td>"
                        }
                        document.getElementById("tab_pac").innerHTML += "</tr>";
                        contador++;
                    }
                    document.getElementById("art_mostrar").innerHTML += "</table>";
                }
            }
            objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            objAjax.send("mostrar=" + 3);
        }
            break;
    }
}
//enseñará la información solcitida, con el valor 0 mostrará las citas atendidas, con el valor 1 mostrará las citas pendientes, con el valor 2 mostrará las citas del propio paciente y con el valor 3 mostrará los pacientes(asistente/médico)

function atender(opcion) {
    objAjax = GetXMLHTTP();
    objAjax.open("POST", "php.php", true);
    if (opcion == true) {
        objAjax.onreadystatechange = function () {
            if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                document.getElementById("form_login").style.display = "none";
                document.getElementById("art_mostrar").style.display = "block";
                document.getElementById("art_mostrar").innerHTML = "Cita Atendida";
                document.getElementById("art_mostrar").innerHTML += objAjax.responseText;
            }
        }

        objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        objAjax.send("atender=" + document.getElementById("obs").value + "&zona=" + 1);
    }
    else {
        objAjax.onreadystatechange = function () {
            if ((objAjax.readyState == 4) && (objAjax.status == 200)) {
                var cita = JSON.parse(this.responseText);
                document.getElementById("art_mostrar").innerHTML = "<h1>ATENDER CITA</h1>";
                document.getElementById("art_mostrar").innerHTML += "<table id='at_cit' name='at_cit'>";
                document.getElementById("at_cit").innerHTML = "<tr id='paciente'>";
                document.getElementById("paciente").innerHTML = "<td>Paciente</td>";
                document.getElementById("paciente").innerHTML += "<td>" + cita[0][0] + " " + cita[0][1] + "</td>";
                document.getElementById("at_cit").innerHTML += "</tr>";
                document.getElementById("at_cit").innerHTML += "<tr id='observaciones'>";
                document.getElementById("observaciones").innerHTML = "<td>Observaciones</td>";
                document.getElementById("observaciones").innerHTML += "<td><textarea id='obs' name='obs'>" + cita[0][2] + "</textarea></td>";
                document.getElementById("at_cit").innerHTML += "</tr>";
                document.getElementById("at_cit").innerHTML += "<tr id='enviar'>";
                document.getElementById("enviar").innerHTML = "<td><button type='button' id='at_but' name='at_but' onclick='atender(true)'>Enviar</button></td>";
                document.getElementById("at_cit").innerHTML += "</tr>";
                document.getElementById("art_mostrar").innerHTML += "</table>";
            }
        }

        objAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        objAjax.send("atender=" + opcion + "&zona=" + 0);
    }


}
//con un valor numérico mostrará la ventana de entrada de información asignada a la ID mandada, y con el valor(actualizar) mandará la informaicón a php


function validar(opcion) {
    switch (opcion) {
        case 0: {
            var mail = document.getElementById("med_mail").value;
            var patron = /[\w]+@{1}[\w]+\.[a-z]{2,3}/;
            if (!patron.test(mail)) {
                document.getElementById("sp_mail_med").style.display = "block";
                document.getElementById("add_med_but").disabled = true;
            }
            else {
                document.getElementById("sp_mail_med").style.display = "none";
                document.getElementById("add_med_but").disabled = false;
            }
        }
            break;
        case 1: {
            var nombrecompleto = document.getElementById("pac_name").value + " " + document.getElementById("pac_apell").value;
            var patron = /^([a-z ñáéíóú]{2,60})$/i;
            if (!patron.test(nombrecompleto)) {
                document.getElementById("sp_nombre_pac").style.display = "block";
                document.getElementById("add_pac_but").disabled = true;
            }
            else {
                document.getElementById("sp_nombre_pac").style.display = "none";
                document.getElementById("add_pac_but").disabled = false;
            }
        }
            break;
        case 2: {
            var nombrecompleto = document.getElementById("med_name").value + " " + document.getElementById("med_apell").value;
            var patron = /^([a-z ñáéíóú]{2,60})$/i;
            if (!patron.test(nombrecompleto)) {
                document.getElementById("sp_nombre_med").style.display = "block";
                document.getElementById("add_med_but").disabled = true;
            }
            else {
                document.getElementById("sp_nombre_med").style.display = "none";
                document.getElementById("add_med_but").disabled = false;
            }
        }
            break;
        case 3: {
            var especialidad = document.getElementById("med_esp").value;
            var patron = /^([a-zñáéíóú]{2,60})$/i;
            if (!patron.test(especialidad)) {
                document.getElementById("sp_esp_med").style.display = "block";
                document.getElementById("add_med_but").disabled = true;
            }
            else {
                document.getElementById("sp_esp_med").style.display = "none";
                document.getElementById("add_med_but").disabled = false;
            }
        }
            break;
        case 4: {
            var telefono = document.getElementById("med_tel").value;
            var patron = /^[9|6]{1}([\d]{2}[-]*){3}[\d]{2}$/;
            if (!patron.test(telefono)) {
                document.getElementById("sp_esp_tel").style.display = "block";
                document.getElementById("add_med_but").disabled = true;
            }
            else {
                document.getElementById("sp_esp_tel").style.display = "none";
                document.getElementById("add_med_but").disabled = false;
            }

        }
            break;
        case 5: {
            var dni = document.getElementById("pac_dni").value;
            var patron = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]$/i;
            if (!patron.test(dni)) {
                document.getElementById("sp_id_pac_val").style.display = "block";
                document.getElementById("add_pac_but").disabled = true;
            }
            else {
                document.getElementById("sp_id_pac_val").style.display = "none";
                document.getElementById("add_pac_but").disabled = false;
            }
        }
            break;
        case 6: {
            var dni = document.getElementById("med_dni").value;
            var patron = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKE]$/i;
            if (!patron.test(dni)) {
                document.getElementById("sp_id_med_val").style.display = "block";
                document.getElementById("add_med_but").disabled = true;
            }
            else {
                document.getElementById("sp_id_med_val").style.display = "none";
                document.getElementById("add_med_but").disabled = false;
            }
        }
            break;
    }
}
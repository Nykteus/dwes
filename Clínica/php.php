<?php
function inicio()
{
    switch ($_SESSION['rol'][0]) {
        case "Administrador": {
                echo ("<div id='div_rol'><h1>Administrador</h1>");
                echo ("<p><button type='button' id='alta_pac' name='alta_pac' onclick='alta(0)'>Alta Paciente</button></p>");
                echo ("<p><button type='button' id='alta_med' name='alta_med' onclick='alta(1)'>Alta Médico</button></p>");
                echo ("<p><button type='button' id='logout' name='logout' onclick='logout()'>Cerrar Sesión</button></p>");
                echo ("</div>");
            }
            break;
        case "Medico": {
                echo ("<div id='div_rol'><h1>Medico</h1>");
                echo ("<p><button type='button' id='med_citas_aten' name='med_citas_aten' onclick='mostrar(0)'>Ver Citas Atendidas</button></p>");
                echo ("<p><button type='button' id='citas_pen' name='citas_pen' onclick='mostrar(1)'>Ver Citas Pendientes</button></p>");
                echo ("<p><button type='button' id='med_pac' name='med_pac' onclick='mostrar(3)'>Ver Pacientes</button></p>");
                echo ("<p><button type='button' id='logout' name='logout' onclick='logout()'>Cerrar Sesión</button></p>");
                echo ("</div>");
            }
            break;
        case "Asistente": {
                echo ("<div id='div_rol'><h1>Asistente</h1>");
                echo ("<p><button type='button' id='asis_citas_aten' name='asis_citas_aten' onclick='mostrar(0)'>Ver Citas Atendidas</button></p>");
                echo ("<p><button type='button' id='citas_pen' name='citas_pen' onclick='alta(2)'>Nueva Cita</button></p>");
                echo ("<p><button type='button' id='alta_pac' name='alta_pac' onclick='alta(0)'>Alta Paciente</button></p>");
                echo ("<p><button type='button' id='asis_pac' name='asis_pac' onclick='mostrar(3)'>Ver Pacientes</button></p>");
                echo ("<p><button type='button' id='logout' name='logout' onclick='logout()'>Cerrar Sesión</button></p>");
                echo ("</div>");
            }
            break;
        case "Paciente": {
                echo ("<div id='div_rol'><h1>Paciente</h1>");
                echo ("<p><button type='button' id='citas_pac' name='citas_pac' onclick='mostrar(2)'>Ver Citas</button></p>");
                echo ("<p><button type='button' id='logout' name='logout' onclick='logout()'>Cerrar Sesión</button></p>");
                echo ("</div>");
            }
            break;
    }
}
session_start();
if (isset($_POST["login"]) && isset($_POST["password"])) {
    $nombre = $_POST["login"];
    $_SESSION['contra'] = $_POST['password'];
    $_SESSION['contra'] = hash('sha1', "$_SESSION[contra]");
    $contra = $_SESSION['contra'];
    $conexion = mysqli_connect("localhost", "root", "", "consulta");
    $_SESSION['rol'] = mysqli_fetch_array(mysqli_query($conexion, "SELECT usutipo FROM usuarios WHERE usuLogin = '$nombre' AND usuPassword = '$contra'"));
    if (isset($_SESSION['rol'])) {
        if ($_SESSION['rol'][0] == "Paciente") {
            $_SESSION['dnipac'] = mysqli_fetch_array(mysqli_query($conexion, "SELECT dniUsu FROM usuarios WHERE usuLogin = '$nombre' AND usuPassword = '$contra'"));
        }
        inicio();
    } else {
        echo (1);
    }
    mysqli_close($conexion);
} else {
    if (isset($_POST["especialidad"])) {
        $dni = $_POST["dni"];
        $nombre = $_POST["nombre"];
        $apellidos = $_POST["apellidos"];
        $especialidad = $_POST["especialidad"];
        $mail = $_POST["mail"];
        $telefono = $_POST["telefono"];
        $usuario = $_POST["usuario"];
        $pass = hash('sha1', "$_POST[contra]");
        $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
        mysqli_query($conexion, "INSERT INTO medicos (dniMed, medNombres, medApellidos, medEspecialidad, medTelefono, medCorreo) VALUES ('$dni', '$nombre', '$apellidos', '$especialidad', '$telefono', '$mail')");
        mysqli_query($conexion, "INSERT INTO usuarios (dniUsu, usuLogin, usuPassword, usuEstado, usutipo) VALUES ('$dni', '$usuario', '$pass', 'Activo', 'Medico')");
        inicio();
        mysqli_close($conexion);
    } else {
        if (isset($_POST["sexo"])) {
            $dni = $_POST["dni"];
            $nombre = $_POST["nombre"];
            $apellidos = $_POST["apellidos"];
            $fecha = $_POST["fecha"];
            $sexo = $_POST["sexo"];
            $usuario = $_POST["usuario"];
            $pass = hash('sha1', "$_POST[contra]");
            $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
            mysqli_query($conexion, "INSERT INTO pacientes (dniPac, pacNombres, pacApellidos, pacFechaNacimiento, pacSexo) VALUES ('$dni', '$nombre', '$apellidos', '$fecha', '$sexo')");
            mysqli_query($conexion, "INSERT INTO usuarios (dniUsu, usuLogin, usuPassword, usuEstado, usutipo) VALUES ('$dni', '$usuario', '$pass', 'Activo', 'Paciente')");
            inicio();
            mysqli_close($conexion);
        } else {
            if (isset($_POST["seleccion"])) {
                switch ($_POST["seleccion"]) {
                }
            } else {
                if (isset($_POST["mostrar"])) {
                    switch ($_POST["mostrar"]) {
                        case 0: {
                                $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                                $resultados = mysqli_fetch_all(mysqli_query($conexion, "SELECT citFecha, citHora, pacNombres, pacApellidos, medNombres, medApellidos, conNombre, citEstado, idCita FROM `citas` INNER JOIN pacientes on citas.citPaciente=pacientes.dniPac INNER JOIN medicos on citas.citMedico=medicos.dniMed INNER JOIN consultorios on citas.citConsultorio=consultorios.idConsultorio WHERE citEstado='Atendido'"));
                                $json = json_encode($resultados);
                                exit($json);
                                mysqli_close($conexion);
                            }
                            break;
                        case 1: {
                                $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                                $resultados = mysqli_fetch_all(mysqli_query($conexion, "SELECT citFecha, citHora, pacNombres, pacApellidos, medNombres, medApellidos, conNombre, citEstado, idCita FROM `citas` INNER JOIN pacientes on citas.citPaciente=pacientes.dniPac INNER JOIN medicos on citas.citMedico=medicos.dniMed INNER JOIN consultorios on citas.citConsultorio=consultorios.idConsultorio WHERE citEstado='Asignado'"));
                                $json = json_encode($resultados);
                                exit($json);
                                mysqli_close($conexion);
                            }
                            break;
                        case 2: {
                                $dnipac = $_SESSION['dnipac'][0];
                                $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                                $resultados = mysqli_fetch_all(mysqli_query($conexion, "SELECT citFecha, citHora, pacNombres, pacApellidos, medNombres, medApellidos, conNombre, citEstado, CitObservaciones FROM `citas` INNER JOIN pacientes on citas.citPaciente=pacientes.dniPac INNER JOIN medicos on citas.citMedico=medicos.dniMed INNER JOIN consultorios on citas.citConsultorio=consultorios.idConsultorio WHERE citPaciente='$dnipac'"));
                                $json = json_encode($resultados);
                                exit($json);
                                mysqli_close($conexion);
                            }
                            break;
                        case 3: {
                                $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                                $resultados = mysqli_fetch_all(mysqli_query($conexion, "SELECT dniPac AS 'Identificacion', pacNombres AS 'Nombre', pacApellidos AS 'Apellido', pacFechaNacimiento AS 'Fecha de Nacimiento', pacSexo AS 'Sexo' FROM pacientes"));
                                $json = json_encode($resultados);
                                exit($json);
                                mysqli_close($conexion);
                            }
                            break;
                    }
                } else {
                    if (isset($_POST["cita"])) {
                        switch ($_POST["cita"]) {
                            case 0: {
                                    $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                                    $resultado = mysqli_query($conexion, "SELECT dniPac, pacNombres, pacApellidos FROM pacientes");
                                    $resultado1 = mysqli_query($conexion, "SELECT dniMed, medNombres, medApellidos FROM medicos");
                                    $resultado2 = mysqli_query($conexion, "SELECT idConsultorio, conNombre FROM consultorios");
                                    $final = ["Pacientes" => mysqli_fetch_all($resultado), "Medicos" => mysqli_fetch_all($resultado1), "Consultorios" => mysqli_fetch_all($resultado2)];
                                    $json = json_encode($final);
                                    exit($json);
                                    mysqli_close($conexion);
                                }
                                break;
                            case 1: {
                                    $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                                    $resultados = mysqli_fetch_all(mysqli_query($conexion, "SELECT dniMed, medNombres, medApellidos FROM medicos"));
                                    $json = json_encode($resultados);
                                    exit($json);
                                    mysqli_close($conexion);
                                }
                                break;
                            case 2: {
                                    $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                                    $resultados = mysqli_fetch_all(mysqli_query($conexion, "SELECT idConsultorio, conNombre FROM consultorios"));
                                    $json = json_encode($resultados);
                                    exit($json);
                                    mysqli_close($conexion);
                                }
                                break;
                        }
                    } else {
                        if (isset($_POST["pacientecita"])) {
                            $dnipaciente = $_POST["pacientecita"];
                            $fecha = $_POST["fechacita"];
                            $hora = $_POST["hora"];
                            $dnimedico = $_POST["medicocita"];
                            $consultorio = $_POST["consultoriocita"];
                            $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                            mysqli_query($conexion, "INSERT INTO citas (idCita, citFecha, citHora, citPaciente, citMedico, citConsultorio, citEstado, citObservaciones) VALUES (NULL, '$fecha', '$hora', '$dnipaciente', '$dnimedico', '$consultorio', 'Asignado', '')");
                            inicio();
                            mysqli_close($conexion);
                        } else {
                            if (isset($_POST["inicio"])) {
                                if (isset($_SESSION['rol'][0])) {
                                    inicio();
                                } else {
                                    echo (0);
                                }
                            } else {
                                if (isset($_POST["atender"])) {
                                    if ($_POST["zona"] == 0) {
                                        $id = $_SESSION['idcita'] = $_POST["atender"];
                                        $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                                        $resultados = mysqli_fetch_all(mysqli_query($conexion, "SELECT pacNombres, pacApellidos, citObservaciones FROM citas INNER JOIN pacientes ON citas.citPaciente=pacientes.dniPac WHERE idCita='$id'"));
                                        $json = json_encode($resultados);
                                        exit($json);
                                        mysqli_close($conexion);
                                    } else {
                                        $observacion = $_POST["atender"];
                                        $id = $_SESSION['idcita'];
                                        $conexion = mysqli_connect("localhost", strtolower($_SESSION['rol'][0]), strtolower($_SESSION['rol'][0]), "consulta");
                                        $resultados = mysqli_query($conexion, "UPDATE citas SET CitObservaciones='$observacion', citEstado='Atendido' WHERE idCita='$id'");
                                        inicio();
                                        mysqli_close($conexion);
                                    }
                                } else {
                                    if (isset($_POST["buscalogin"])) {
                                        $log = $_POST["buscalogin"];
                                        switch ($_POST["eleccion"]) {
                                            case 0: {
                                                    $conexion = mysqli_connect("localhost", "root", "", "consulta");
                                                    $resulog = mysqli_fetch_array(mysqli_query($conexion, "SELECT usuLogin FROM usuarios WHERE usuLogin='$log'"));
                                                    if (isset($resulog)) {
                                                        echo (1);
                                                    } else {
                                                        echo (0);
                                                    }
                                                    mysqli_close($conexion);
                                                }
                                                break;
                                            case 1: {
                                                    $conexion = mysqli_connect("localhost", "root", "", "consulta");
                                                    $resulog = mysqli_fetch_array(mysqli_query($conexion, "SELECT dniUsu FROM usuarios WHERE dniUsu='$log'"));
                                                    if (isset($resulog)) {
                                                        echo (0);
                                                    } else {
                                                        echo (1);
                                                    }
                                                    mysqli_close($conexion);
                                                }
                                                break;
                                            case 2: {
                                                    $conexion = mysqli_connect("localhost", "root", "", "consulta");
                                                    $resulog = mysqli_fetch_array(mysqli_query($conexion, "SELECT usuLogin FROM usuarios WHERE usuLogin='$log'"));
                                                    if (isset($resulog)) {
                                                        echo (0);
                                                    } else {
                                                        echo (1);
                                                    }
                                                    mysqli_close($conexion);
                                                }
                                                break;
                                            case 3: {
                                                    $conexion = mysqli_connect("localhost", "root", "", "consulta");
                                                    $resulog = mysqli_fetch_array(mysqli_query($conexion, "SELECT dniUsu FROM usuarios WHERE dniUsu='$log'"));
                                                    if (isset($resulog)) {
                                                        echo (0);
                                                    } else {
                                                        echo (1);
                                                    }
                                                    mysqli_close($conexion);
                                                }
                                                break;
                                            case 4: {
                                                    $conexion = mysqli_connect("localhost", "root", "", "consulta");
                                                    $resulog = mysqli_fetch_array(mysqli_query($conexion, "SELECT usuLogin FROM usuarios WHERE usuLogin='$log'"));
                                                    if (isset($resulog)) {
                                                        echo (0);
                                                    } else {
                                                        echo (1);
                                                    }
                                                    mysqli_close($conexion);
                                                }
                                                break;
                                        }
                                    }
                                    else{
                                        if(isset($_POST["logout"])){
                                            session_destroy();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
?>
